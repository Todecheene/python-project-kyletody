import arcpy

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Flood Plain Vegetation Tool"
        self.alias = "FPVT"

        # List of tool classes associated with this toolbox
        self.tools = [FloodPlainVegetationTool]
        
class FloodPlainVegetationTool(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Flood Plain Vegetation Tool"
        self.description = "FPVT"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        # first parameter
        param0 = arcpy.Parameter(
            displayName="DEM",
            name="DEM",
            datatype="DERasterDataset",
            parameterType="Required",
            direction="Input")

        # second parameter
        param1 = arcpy.Parameter(
            displayName="NHD",
            name="NHD",
            datatype="DEFeatureClass",
            parameterType="Required",
            direction="Input")
        param1.filter.list = ["Polyline"]
         
        # third parameter
        param2 = arcpy.Parameter(
            displayName="Buffer",
            name="Buffer",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        param2.value = 25
        
        # fourth parameter
        param3 = arcpy.Parameter(
            displayName="Slope",
            name="Slope",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        param3.value = 15
        
        # fifth parameter
        param4 = arcpy.Parameter(
            displayName="Output",
            name="Output",
            datatype="DEFeatureClass",
            parameterType="Required",
            direction="Output")
        
        # return a list of parameters
        params = [param0, param1, param2, param3, param4]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""

        # get parameters
        data_DEM = parameters[0].valueAsText
    	data_NHD = parameters[1].valueAsText
        data_buffer = parameters[2].valueAsText
        data_slope = parameters[3].valueAsText
        data_output = parameters[4].valueAsText

        # Allow overwrite
        
        arcpy.env.overwriteOutput = True

        # Set coordinate space
        
        arcpy.CheckOutExtension("spatial")

        # Import Digital Elevation Model (DEM) and National Hydrological Dataset (NHD) 
        
        # Creating Buffer analysis 
        arcpy.AddMessage("Creating Buffer")
        # Set a buffer region for the drainage system to the elevation data
        
        arcpy.Buffer_analysis( data_NHD, "buffer_tool", data_buffer, "FULL", "ROUND", "ALL")
        
        # Creating Slope Analysis
        arcpy.AddMessage("Creating Slope")
        
        slope_input = arcpy.sa.Slope( data_DEM, "DEGREE", 0.3043)
        
        # Creating Slope and Buffer analysis
        arcpy.AddMessage("Slope Buffer Analysis")
        
        slope_buff = arcpy.sa.ExtractByMask(slope_input, "buffer_tool")
        
        valley_input = arcpy.sa.Reclassify( slope_buff, "VALUE", "0 {0} 1; {0} 100 NODATA".format(data_slope), "NODATA")
        
        arcpy.RasterToPolygon_conversion(valley_input, "data_output", "SIMPLIFY")
        
        messages.addMessage('Done!')
        return



