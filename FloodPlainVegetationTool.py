#Flood Plain Vegetation Tool
#Kyle Todecheene
#November 28, 2018
#WILD 4950 - Python for ArcGIS
#Project

#import operating tools 
import arcpy
from arcpy import env
import os
import sys

#set workspace

arcpy.env.workspace = r'E/Python-Project'
#allow overwrite
arcpy.env.overwriteOutput = True

#set coordinate space
arcpy.CheckOutExtension("spatial")
NDH_network = arcpy.Describe('NDH_input').spatialReference

#set a buffer region for the drainage system to the elevation data
arcpy.MakeFeatureLayer_management(NHD_input, [] )

#import Digital Elevation Model (DEM) and National Hydrological Dataset (NHD) 
slope_grade = Slope(DEM_input, "DEGREE", "")
buffer_tool = Buffer_analysis(NHD_input, Valley_Out, [])

#clip out the imported image of National Agriculture Imagery Program NAIP
Clip_analysis(Image_input, Valley_Out, [])

DEM_input = parameters[0].valueAsText
NDH_input = parameters[1].valueAsText
Buffer_input = parameters[2].valueAsText
Image_input = parameters[3].valueAsText
Output = parameters[4].valueAsText